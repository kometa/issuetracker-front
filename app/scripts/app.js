'use strict';

angular
  .module('issueTrackerApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngTable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/list', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
      })
        .when('/issue/:issueId', {
            templateUrl: 'views/issue.html',
            controller: 'IssueCtrl'
        })
        .when('/add', {
            templateUrl: 'views/add.html',
            controller: 'AddCtrl'
        })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function($rootScope) {


        $rootScope.statusToString = function($id){

            for(var i = 0; i < $rootScope.statuses.length; ++i){
                if($rootScope.statuses[i].id == $id)
                    return $rootScope.statuses[i].name;
            }
            return 'Nieznany';
        }
        $rootScope.priorityToString = function($id){

            for(var i = 0; i < $rootScope.priorities.length; ++i){
                if($rootScope.priorities[i].id == $id)
                    return $rootScope.priorities[i].name;
            }
            return 'Nieznany';
        }

        $rootScope.priorities = [
            {
                'id' : 1,
                'name' : 'Niski'
            },
            {
                'id' : 2,
                'name' : 'Średni'
            },
            {
                'id' : 3,
                'name' : 'Wysoki'
            }
        ]

        $rootScope.statuses = [
            {
                'id' : 1,
                'name' : 'Stworzony'
            },
            {
                'id' : 2,
                'name' : 'Otwarty'
            },
            {
                'id' : 3,
                'name' : 'W trakcie'
            },
            {
                'id' : 4,
                'name' : 'Wstrzymany'
            },
            {
                'id' : 5,
                'name' : 'Zweryfikowany'
            },
            {
                'id' : 6,
                'name' : 'Zamknięty'
            },
            {
                'id' : 7,
                'name' : 'Anulowany'
            }
        ]



    });

