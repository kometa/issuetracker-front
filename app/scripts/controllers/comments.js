'use strict';

angular.module('issueTrackerApp')
    .controller('CommentsCtrl', function ($scope, $rootScope, $http, $filter, ngTableParams, Comment, Users, $timeout, $routeParams) {
        $scope.isLoading = false;
        $scope.comments = [];
        $scope.addNewComment = addNewComment;

        loadUsers();
        loadComments();

        function setTable() {
            $scope.tableParams1 = new ngTableParams({
                page: 1,            // show first page
                count: 5,          // count per page
                sorting: {
                    data: 'desc'
                }
            }, {
                total: $scope.comments.length, // length of data
                counts: [5, 10, 20, 40],
                getData: function($defer, params) {
                    var data = $scope.comments;
                    var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
        };

        function loadComments() {
            $scope.isLoading = true;
            Comment.query({idZgloszenia: $routeParams.issueId}).$promise.then(
                function( response ) {
                    $scope.comments = response;
                    setTable();
                    $scope.isLoading = false;
                    console.log(response);
                },
                function( error ) {
                    alert( "Cannot load comments");
                })
        };

        function addNewComment() {
            console.log($scope.newComment);

            var newComment = {
                idOsoba: $scope.formData.idOsoba,
                tresc: $scope.newComment,
                idZgloszenie: $routeParams.issueId
            };

            Comment.save(newComment).$promise.then(function (response) {
                    newComment.data = Date.now();    // FIXME data z response

                    for (var i=0; $scope.people.length; ++i) {
                        if ($scope.people[i].idOsoba === newComment.idOsoba) {
                            newComment.osobaKomentujaca = $scope.people[i].imie + ' ' + $scope.people[i].nazwisko;
                            break;
                        }
                    }
                    $scope.comments.unshift(newComment);
                    $scope.tableParams1.reload();
                },
                function (error) {
                    console.log("Komentarz nie zostal dodany");
                }
            );

            $scope.newComment = "";
        }

        function loadUsers() {
            Users.query().$promise.then(function (response) {
                            $scope.people = response;
                            $scope.formData.idOsoba = response[0].idOsoba;
                            console.log("Users load")
                        },
                        function (error) {
                            console.log("Something went wrong!");
                        }
                );
        };
});