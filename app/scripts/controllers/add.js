'use strict';

angular.module('issueTrackerApp')
    .controller('AddCtrl', function ($scope, $rootScope, $http, issueDetail, Users, $timeout, $location) {



        $scope.people = $rootScope.people;
        $scope.newId = 0;
        $scope.formData = {
            nazwa: "",
            opis: "",
            priorytet: 1,
            idOsobyZglaszajacej: 1,
            idOsobyWykonujacej: 1
        };

        loadUsers();


        $scope.submit = function () {


            $http({
                method: 'POST',
                url: 'http://kursdoge.pl:9876/issue-tracker-0.1.0/issue',
                data: $scope.formData,  // pass in data as strings
                headers: { 'Content-Type': 'application/json' }  // set the headers so angular passing info as form data (not request payload)
            })
                .success(function (data, status, headers, config) {

                    if (status == 201) {
                        $location.path('/issue/' + data.idZgloszenie);

                    }
                    //   console.log(data);

                    if (!data.success) {

                    } else {

                    }
                });
        };

        function loadUsers(){

            Users.
                query().
                $promise.
                then(
                function (response) {
                    $timeout(function () {
                            $scope.people = response;
                            console.log("USers load")
                        },
                        function (error) {
                            // If something goes wrong with a JSONP request in AngularJS,
                            // the status code is always reported as a "0". As such, it's
                            // a bit of black-box, programmatically speaking.
                            console.log("Something went wrong!");
                        }
                    );
                });
        };
    });
