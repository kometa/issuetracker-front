'use strict';

angular.module('issueTrackerApp')
    .controller('IssueCtrl', function ($scope, $rootScope, $routeParams, issueDetail, Users, $http, $timeout) {

        $scope.isLoading = true;

        $scope.issueId = $routeParams.issueId;
        $scope.author = 'Ładowanie';
        $scope.appointed = 'Ładowanie';
        $scope.issueStatus = 'Ładowanie';
        $scope.issuePriority = 'Ładowanie';


        $scope.formData = {
            priorityToBe: -1,
            statusToBe: -1,
            userTobe: -1
        };

        $scope.submitPriorityChange = function(){
            console.log(1);
        }

        loadRemoteData();

        function loadRemoteData() {

            issueDetail.
                get({'issueId' : $routeParams.issueId}).
                $promise.
                then(
                function (response) {
                    $timeout(function () {
                        $scope.issue = response;
                        $scope.issueStatus = $rootScope.statusToString($scope.issue.status)
                        $scope.issuePriority = $rootScope.priorityToString($scope.issue.priorytet)
                        $scope.isLoading = false;
                        loadUsers();
                    }, 500);
                },
                function (error) {

                    console.log("Something went wrong!");
                }
            );
        };

        function loadUsers(){

            Users.
                query().
                $promise.
                then(
                function (response) {
                    $timeout(function () {
                        $scope.people = response;

                        for(var i = 0; i< response.length; ++i){
                            if(response[i].idOsoba == $scope.issue.idOsobyZglaszajacej)
                                $scope.author = response[i].imie + ' ' + response[i].nazwisko;

                            if(response[i].idOsoba == $scope.issue.idOsobyWykonujacej)
                                $scope.appointed = response[i].imie + ' ' + response[i].nazwisko;
                        }
                    }, 500);
                },
                function (error) {

                    console.log("Something went wrong!");
                }
            );

        };


        $scope.submitPriorityChange = function () {

            $http({
                method: 'PUT',
                url: 'http://kursdoge.pl:9876/issue-tracker-0.1.0/issue/' + $scope.issue.idZgloszenie,
                data: {
                    nazwa: $scope.issue.nazwa,
                    opis: $scope.issue.opis,
                    priorytet: $scope.issue.priorytet
                },  // pass in data as strings
                headers: { 'Content-Type': 'application/json' }
            })
                .success(function (data) {

                    $('#changePriorityModal').modal('hide')
                    $scope.issuePriority = $rootScope.priorityToString($scope.issue.priorytet)

                }).error(function (data, status, headers, config) {
                    console.log(status);

                });
        };

        $scope.submitUserChange = function () {

            $http({
                method: 'PATCH',
                url: 'http://kursdoge.pl:9876/issue-tracker-0.1.0/issue/' + $scope.issue.idZgloszenie,
                data: {
                    idOsobyWykonujacej: $scope.issue.idOsobyWykonujacej
                },
                headers: { 'Content-Type': 'application/json' }
            })
                .success(function (data) {

                    $('#changeUserModal').modal('hide')

                    for (var i = 0; i < $scope.people.length; ++i) {

                        if ($scope.issue.idOsobyWykonujacej == $scope.people[i].idOsoba)
                            $scope.appointed = $scope.people[i].imie + ' ' + $scope.people[i].nazwisko;
                    }


                }).error(function (data, status, headers, config) {
                    console.log(status);
//                    $scope.wrongStatusChange = true;
                });
        };


        $scope.submitStatusChange = function () {

            $http({
                method: 'POST',
                url: 'http://kursdoge.pl:9876/issue-tracker-0.1.0/statusoperation',
                data: {
                    idZgloszenie: $scope.issue.idZgloszenie,
                    operationType: $scope.issue.status
                },  // pass in data as strings
                headers: { 'Content-Type': 'application/json' }
            })
                .success(function (data) {
                    $scope.wrongStatusChange = false;

                    $('#changeStatusModal').modal('hide')
                    $scope.issueStatus = $rootScope.statusToString($scope.issue.status)

                }).error(function (data, status, headers, config) {
                    console.log(status);
                    $scope.wrongStatusChange = true;
                });
        };


    });
