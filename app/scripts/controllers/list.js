'use strict';

angular.module('issueTrackerApp')
    .controller('ListCtrl', function ($scope, $rootScope, $http, $filter, ngTableParams, Issue, $timeout) {

    $scope.isLoading = false;
    $scope.issues = [];
    var issues = $scope.issues;

    loadData();

    function setTable() {
        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 5,          // count per page
            sorting: {
                priorytet: 'desc'
            }
        }, {
            total: $scope.issues.length, // length of data
            counts: [5, 10, 20, 40],
            getData: function($defer, params) {
                var data = $scope.issues;
                var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data;
                var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
    };

    function addPrioAndStatusStrToIssues() {
        for (var i=0; i<$scope.issues.length; ++i) {
            $scope.issues[i].prioStr = $rootScope.priorityToString($scope.issues[i].priorytet);
            $scope.issues[i].statusStr = $rootScope.statusToString($scope.issues[i].status);
        }
    };

    // I load the remote data.
    function loadData() {
        $scope.isLoading = true;
        Issue.query().$promise.then(
            function( response ) {
//                $timeout(function(){
                    $scope.isLoading = false;
                    $scope.issues = response;
                    addPrioAndStatusStrToIssues();
                    setTable();
//                }, 500);
            },
            function( error ) {
                // If something goes wrong with a JSONP request in AngularJS,
                // the status code is always reported as a "0". As such, it's
                // a bit of black-box, programmatically speaking.
                alert( "Something went wrong!" );
            }
        );
    }
  });
