'use strict';

angular.module('issueTrackerApp')
    .factory('issueDetail', function ($resource) {
        return $resource(
            'http://kursdoge.pl:9876/issue-tracker-0.1.0/issue/:issueId',
          //  '/data/issue:issueId.json',
            { bookId: '@issueId' }, {
                get: {
                    method: 'GET',
                    params: { bookId: '@issueId' },
                    isArray: false
                }
            } );




    });
