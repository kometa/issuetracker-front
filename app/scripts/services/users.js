'use strict';

angular.module('issueTrackerApp')
    .factory('Users', function ($resource) {
        return $resource(
            'http://kursdoge.pl:9876/issue-tracker-0.1.0/user'
//             '/data/users.json'
        );
    });
