'use strict';

angular.module('issueTrackerApp')
    .factory('Comment', function ($resource) {
        return $resource(
//            'data/comments.json'
        'http://kursdoge.pl:9876/issue-tracker-0.1.0/comment', {}, {
            query: {method:'GET', params: {idZgloszenia: '@id'}, isArray:true}
        }
        );
    });