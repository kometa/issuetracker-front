'use strict';

angular.module('issueTrackerApp')
  .factory('Issue', function ($resource) {
    return $resource(
//        'data/issues1.json'
        'http://kursdoge.pl:9876/issue-tracker-0.1.0/issue'
    );
  });
