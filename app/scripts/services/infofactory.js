'use strict';

angular.module('issueTrackerApp')
    .factory('infoFactory', function () {
        // Service logic
        // ...

        var meaningOfLife = 42;

        // Public API here
        return {
            someMethod: function () {
                return meaningOfLife;
            }
        };
    });
