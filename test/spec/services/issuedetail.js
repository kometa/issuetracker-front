'use strict';

describe('Service: IssueDetail', function () {
    var mockIssueDetailResource, $httpBackend;
    beforeEach(angular.mock.module('issueTrackerApp'));

    beforeEach(function () {
        angular.mock.inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            mockIssueDetailResource = $injector.get('issueDetail');
        })
    });

    describe('getIssue', function () {
        it('should call get with issueId', inject(function (issueDetail) {
            $httpBackend.expectGET('http://kursdoge.pl:9876/issue-tracker-0.1.0/issue/42')
                .respond({"idZgloszenie": 42, "opis": "43", "nazwa": "test5", "data": 1401395133000, "priorytet": 2, "status": 2, "dataZmianyStanu": 1401395133000, "idOsobyZglaszajacej": 1, "idOsobyWykonujacej": 1, "osobaZglaszajaca": {
                    "idOsoba": 1,
                    "imie": "Jakub",
                    "nazwisko": "Strzalek",
                    "mail": "strzala@gmail.com",
                    "aktywny": false
                }, "osobaWykonujaca": {
                    "idOsoba": 1,
                    "imie": "Jakub",
                    "nazwisko": "Strzalek",
                    "mail": "strzala@gmail.com",
                    "aktywny": false
                }});

            var result = mockIssueDetailResource.get({'issueId' : 42});

            $httpBackend.flush();

            expect(result.idZgloszenie).toEqual(42);
        }));

    });
});