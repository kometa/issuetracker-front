'use strict';

describe('Service: infoFactory', function () {

    // load the service's module
    beforeEach(module('issueTrackerApp'));

    // instantiate service
    var infoFactory;
    beforeEach(inject(function (_infoFactory_) {
        infoFactory = _infoFactory_;
    }));

    it('should do something', function () {
        expect(!!infoFactory).toBe(true);
    });

});
