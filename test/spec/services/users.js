'use strict';

describe('Service: Users', function () {

    var mockUsersResource, $httpBackend;
    beforeEach(angular.mock.module('issueTrackerApp'));

    beforeEach(function () {
        angular.mock.inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            mockUsersResource = $injector.get('Users');
        })
    });

    describe('getUsers', function () {
        it('should call query on Users', inject(function (Users) {
            $httpBackend.expectGET('http://kursdoge.pl:9876/issue-tracker-0.1.0/user')
                .respond(
                [
                    {"idOsoba": 1, "imie": "Jakub", "nazwisko": "Strzalek", "mail": "strzala@gmail.com", "aktywny": false},
                    {"idOsoba": 2, "imie": "Pawel", "nazwisko": "Blicharski", "mail": "blichar@gmail.com", "aktywny": true},
                    {"idOsoba": 3, "imie": "Jedrus", "nazwisko": "Modzelewski", "mail": "qudlatyto@gmail.com", "aktywny": true},
                    {"idOsoba": 4, "imie": "Jaroslaw", "nazwisko": "Kornata", "mail": "jatokor@gmail.com", "aktywny": true},
                    {"idOsoba": 5, "imie": "Jakuj", "nazwisko": "Ziec", "mail": "jziec@gmail.com", "aktywny": true}
                ]
            );

            var result = mockUsersResource.query();

            $httpBackend.flush();

            expect(result.length).toEqual(5);
        }));

    });
});
