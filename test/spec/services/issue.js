'use strict';

describe('Service: Issue', function () {

    var mockIssuesResource, $httpBackend;
    beforeEach(angular.mock.module('issueTrackerApp'));

    beforeEach(function () {
        angular.mock.inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            mockIssuesResource = $injector.get('Issue');
        })
    });

    describe('getIssues', function () {
        it('should call query on Issue', inject(function (Users) {
            $httpBackend.expectGET('http://kursdoge.pl:9876/issue-tracker-0.1.0/issue')
                .respond(
                [
                    {"idZgloszenie":4,"opis":"fdf","nazwa":"fdf","priorytet":1,"status":1,"data":1401393561000,"dataZmianyStanu":946702800000,"idOsobyWykonujacej":1},
                    {"idZgloszenie":5,"opis":"43","nazwa":"43","priorytet":1,"status":1,"data":946702800000,"dataZmianyStanu":946702800000,"idOsobyWykonujacej":3}
                ]
            );

            var result = mockIssuesResource.query();

            $httpBackend.flush();

            expect(result.length).toEqual(2);
        }));

    });
});
